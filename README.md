# React application - IT Talent Program + Uppsala Systemvetare

You can apply to the IT Talent Program 2021 here: [https://jobs.tele2.com/job/Stockholm-Tele2-IT-Talent-Program-2021/635349701/](https://jobs.tele2.com/job/Stockholm-Tele2-IT-Talent-Program-2021/635349701/)
Application period closes on the 31st of January.

More information about the IT Talent Program can be found here: [https://www.tele2.com/career/talent-programs/it-talent-program](https://www.tele2.com/career/talent-programs/it-talent-program)

## Project installation/setup (Please follow these steps before the workshop)

**Install Microsoft Teams on your computer so that you can join the workshop: [https://www.microsoft.com/sv-se/microsoft-365/microsoft-teams/download-app](https://www.microsoft.com/sv-se/microsoft-365/microsoft-teams/download-app)** _(Meeting link will be sent out before the event)_

To get the project on your computer, you can either press the download icon next to "Clone" or press "Clone" and copy the "clone with https"-url and use git to clone it from your terminal.

If you're working on a Mac, please install Xcode on your computer before proceeding. You can find it on the App Store.

1. Install Node.js and npm on your computer [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
   - PC
     - Open CMD and write:
       - node -v
       - npm -v
     - If installed correctly it will display the versions installed.
   - Mac / Linux
     - Open Terminal (or iTerm) and write:
       - node -v
       - npm -v
     - If installed correctly it will display the versions installed.
2. Install Visual Studio Code [https://code.visualstudio.com/download](https://code.visualstudio.com/download)

3. Test if the react application runs:
   - Open the "us-talent-workshop"-folder in Visual Studio Code
   - Open a new terminal within VS Code and write "npm install" and press Enter.
   - After everything has been installed, write "npm start" and press Enter.
   - The application should now start up and be displayed in your default browser.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
