import React from 'react';
//import '../css/Brands.css';
import './Brands.scss';

const Brands = ({ brands }) => {
  const renderBrands = brands.map((brand) => {
    const imgSrcStart = '/images/';
    let imgSrcEnd = '.svg';
    if (
      brand.name === 'Boxer' ||
      brand.name === 'Com Hem' ||
      brand.name === 'Comhem Play'
    )
      imgSrcEnd = '.png';

    const imgSrc = imgSrcStart + brand.imageName + imgSrcEnd;
    return (
      <div className="brand" key={brand.id} id="brands">
        <a href={brand.url} target="_blank" rel="noreferrer">
          <img className="brand-logo" src={imgSrc} alt={brand.imageName}></img>
        </a>
      </div>
    );
  });
  return (
    <div className="brands-wrapper">
      <h2>Brands</h2>
      <div className="flex flex-wrap">{renderBrands}</div>
    </div>
  );
};

export default Brands;
