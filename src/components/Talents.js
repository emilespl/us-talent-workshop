import React, { useEffect, useState } from 'react';
//import '../css/Talents.css';
import './Talents.scss';

const Talents = ({ talents }) => {
  const [limit, setLimit] = useState(4);
  const [isLoading, setIsLoading] = useState(true);

  const loadingBoxes = [...Array(4)];

  const increaseLimit = () => {
    const talentsLeft = talents.length - limit;
    if (talentsLeft >= 4) {
      setLimit(limit + 4);
    } else if (talentsLeft > 0) {
      setLimit(limit + talentsLeft);
    }
  };

  const decreaseLimit = () => {
    setLimit(4);
  };

  const renderLoading = loadingBoxes.map((e, i) => {
    return (
      <div key={i} className="talent-card">
        <div className="loading-talent-image animated-background"></div>
        <div className="talent-info">
          <div className="loading-talent-info animated-background"></div>
          <div className="loading-talent-info animated-background"></div>
          <div className="loading-talent-info animated-background"></div>
        </div>
      </div>
    );
  });

  const getImageSrc = (imgSrc) => {
    if (imgSrc === '') return imgSrc;
    else {
      return `/images/${imgSrc}.png`;
    }
  };

  const renderTalents = talents.slice(0, limit).map((talent) => {
    const imgSrc = getImageSrc(talent.imageName);
    return (
      <div key={talent.id} className="talent-card">
        <div>
          {imgSrc === '' ? (
            <span className="material-icons talent-img">account_circle</span>
          ) : (
            <img
              className="talent-img-box talent-img"
              src={imgSrc}
              alt={talent.imageName}
            />
          )}
        </div>
        <div className="talent-info">
          <p style={{ fontSize: '18px', fontWeight: 'bold' }}>{talent.name}</p>
          <p>{talent.team}</p>
          <p>{talent.education}</p>
        </div>
      </div>
    );
  });

  useEffect(() => {
    if (talents.length) {
      setIsLoading(false);
    }
  }, [talents]);

  return (
    <div className="section" id="talents">
      <h2>Talents & 'The Talentless'</h2>
      <div className="flex flex-wrap">
        {!isLoading ? renderTalents : renderLoading}
      </div>

      {!(limit < talents.length) ? (
        <button
          className={`show-btn`}
          onClick={decreaseLimit}
          disabled={limit < talents.length}
        >
          Show less
        </button>
      ) : (
        <button
          className={`show-btn`}
          onClick={increaseLimit}
          disabled={!(limit < talents.length)}
        >
          Show more
        </button>
      )}
    </div>
  );
};

export default Talents;
