import React from 'react';
import '../css/Movies.css';

const RecommendedMovie = ({ movie }) => {
  return (
    <div className="recommended-block">
      <h2 className="recommended-header">Our recommendation</h2>
      <img src={movie.posterUrl} alt={movie.title} style={{ width: "300px" }} />
      <p style={{ margin: "8px", fontSize: '18px', fontWeight: 'bold' }}>{movie.title}</p>
      <p className="movie-text" >{movie.genre}</p>
      <p className="movie-text" >{movie.description}</p>
    </div>
  )
}

export default RecommendedMovie;