import React, { useEffect, useState } from 'react';
import Course from './Course';
//import '../css/Courses.css';
import './Courses.scss';

const Courses = ({ courses }) => {
  const [isLoading, setIsLoading] = useState(true);

  const renderCourses = courses.map((course) => {
    return <Course course={course} key={course.id} />;
  });

  const loadingBoxes = [...Array(10).keys()].map((i) => i + 1);

  const renderLoading = loadingBoxes.map((e, i) => {
    return (
      <div
        key={i}
        className={`grid-box animated-background grid-box-${e}`}
      ></div>
    );
  });

  useEffect(() => {
    if (courses.length) {
      setIsLoading(false);
    }
  }, [courses]);

  return (
    <div className="section" id="courses">
      <h2>Courses</h2>
      <div className="grid-wrapper">
        {!isLoading ? renderCourses : renderLoading}
      </div>
    </div>
  );
};

export default Courses;
