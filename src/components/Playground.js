import '../App.css';
import '../css/main.css';
import Flex from './Flex';
import Grid from './Grid';

const Playground = () => {
  return (
    <>
      <div className="content">
        <h1>Playground</h1>
        <Flex />
        <Grid />
      </div>
    </>
  )
}

export default Playground;