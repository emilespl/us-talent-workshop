import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import '../../css/Header.css';
import '../../css/Playground.css';

const FlexDirection = ({ onChange, resetValue, reset }) => {
  const [value, setValue] = useState({ value: 'row-fd', label: 'Row' });
  const handleDirection = (e) => {
    setValue(e);
    onChange(e.value);
  };

  const options = [
    { value: 'row-fd', label: 'Row' },
    { value: 'row-reverse-fd', label: 'Row reverse' },
    { value: 'column-fd', label: 'Column' },
    { value: 'column-reverse-fd', label: 'Column reverse' }
  ];

  useEffect(() => {
    if (resetValue) {
      setValue(options[0]);
      reset(false);
    }
  }, [resetValue]);

  return (
    <div className="flex column-fd text-align-l">
      <p className="dropdown-label">Flex direction:</p>
      <Select
        defaultValue={options[0]}
        onChange={handleDirection}
        options={options}
        value={value}
      />
    </div>
  );
};

export default FlexDirection;
