import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import '../../css/Header.css';
import '../../css/Playground.css';

const Wrap = ({ onChange, resetValue, reset }) => {
  const [value, setValue] = useState({ value: 'no-wrap-fw', label: 'No wrap' });
  const handleWrap = (e) => {
    setValue(e);
    onChange(e.value);
  };

  const options = [
    { value: 'no-wrap-fw', label: 'No wrap' },
    { value: 'wrap-fw', label: 'Wrap' },
    { value: 'wrap-reverse-fw', label: 'Wrap reverse' }
  ];

  useEffect(() => {
    if (resetValue) {
      setValue(options[0]);
      reset(false);
    }
  }, [resetValue]);

  return (
    <div className="flex column-fd text-align-l">
      <p className="dropdown-label">Flex wrap:</p>
      <Select
        defaultValue={options[0]}
        onChange={handleWrap}
        options={options}
        value={value}
      />
    </div>
  );
};

export default Wrap;
