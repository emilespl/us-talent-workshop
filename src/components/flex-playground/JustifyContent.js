import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import '../../css/Header.css';
import '../../css/Playground.css';

const JustifyContent = ({ onChange, resetValue, reset }) => {
  const [value, setValue] = useState({
    value: 'flex-start-jc',
    label: 'Flex start'
  });
  const handleJustify = (e) => {
    setValue(e);
    onChange(e.value);
  };

  const options = [
    { value: 'flex-start-jc', label: 'Flex start' },
    { value: 'flex-end-jc', label: 'Flex end' },
    { value: 'center-jc', label: 'Center' },
    { value: 'space-between-jc', label: 'Space between' },
    { value: 'space-around-jc', label: 'Space around' },
    { value: 'space-evenly-jc', label: 'Space evenly' }
  ];

  useEffect(() => {
    if (resetValue) {
      setValue(options[0]);
      reset(false);
    }
  }, [resetValue]);

  return (
    <div className="flex column-fd text-align-l">
      <p className="dropdown-label">Justify content:</p>
      <Select
        defaultValue={options[0]}
        onChange={handleJustify}
        options={options}
        value={value}
      />
    </div>
  );
};

export default JustifyContent;
