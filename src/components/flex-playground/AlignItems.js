import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import '../../css/Header.css';
import '../../css/Playground.css';

const AlignItems = ({ onChange, resetValue, reset }) => {
  const [value, setValue] = useState({
    value: 'flex-start-ai',
    label: 'Flex start'
  });
  const handleAlignItems = (e) => {
    setValue(e);
    onChange(e.value);
  };

  const options = [
    { value: 'flex-start-ai', label: 'Flex start' },
    { value: 'flex-end-ai', label: 'Flex end' },
    { value: 'center-ai', label: 'Center' },
    { value: 'stretch-ai', label: 'Stretch' },
    { value: 'baseline-ai', label: 'Baseline' }
  ];

  useEffect(() => {
    if (resetValue) {
      setValue(options[0]);
      reset(false);
    }
  }, [resetValue]);

  return (
    <div className="flex column-fd text-align-l">
      <p className="dropdown-label">Align items:</p>
      <Select
        defaultValue={options[0]}
        onChange={handleAlignItems}
        options={options}
        value={value}
      />
    </div>
  );
};

export default AlignItems;
