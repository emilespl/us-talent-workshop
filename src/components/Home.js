import '../App.css';
import '../css/main.css';
import spring from '../api/spring';
import { useEffect, useState } from 'react';
import Courses from './Courses';
import Talents from './Talents';
import FunFacts from './FunFacts';

const Home = () => {
  const [talents, setTalents] = useState([]);
  const [courses, setCourses] = useState([]);
  const [funfacts, setFunFact] = useState([]);

  const talentData = async () => {
    const response = await spring.get('/talents/all');
    const { data } = response;
    setTalents(data);
  };

  const courseData = async () => {
    const response = await spring.get('/courses/all');
    const { data } = response;
    setCourses(data);
  };

  const funfactData = async () => {
    const response = await spring.get('/funfacts/all');
    const { data } = response;
    setFunFact(data);
  };

  useEffect(() => {
    // Remove comments if you want to simulate that the API is loading slowly to show animation.
    // setTimeout(() => {
    talentData();
    funfactData();
    courseData();
    // }, 5000);
  }, []);

  return (
    <>
      <div className="content">
        <h1>IT Talent Program</h1>
        <Talents talents={talents} />
        <FunFacts funfacts={funfacts} />
        <Courses courses={courses} />
      </div>
    </>
  );
}

export default Home;