import '../App.css';
import '../css/main.css';
import React, { useState } from 'react';
import { gql, useQuery } from '@apollo/client';
import RecommendedMovie from './RecommendedMovie';
import AllMovies from './AllMovies';
import Select from 'react-select';

const FIND_MOVIE = gql`
  query($id: ID!) {
    findMovie(id: $id) {
      id
      title
      genre
      description
      posterUrl
    }
  }
`;

const MoviePage = () => {
  const [genre, setGenre] = useState('');
  const [id, setId] = useState(6);
  const [value, setValue] = useState({
    value: 'All',
    label: 'All movies'
  });

  const {
    loading: loadingRecommendedMovie,
    error: errorRecommendedMovie,
    data: dataRecommendedMovie
  } = useQuery(FIND_MOVIE, {
    variables: { id }
  });

  if (loadingRecommendedMovie) return <p>Loading...</p>;
  if (errorRecommendedMovie) return <p>Error :(</p>;

  const options = [
    { value: 'All', label: 'All movies' },
    { value: 'Horror', label: 'Horror' },
    { value: 'Kids', label: 'Kids' },
    { value: 'Crime', label: 'Crime' },
    { value: 'Comedy', label: 'Comedy' },
    { value: 'Romance/Drama', label: 'Romance/Drama' },
    { value: 'Action', label: 'Action' },
    { value: 'Western', label: 'Western' },
    { value: 'Sci-fi', label: 'Sci-fi' }
  ];

  return (
    <>
      <div className="content">
        <div className="section">
          <h1>GraphQL - Movies</h1>
          <div>
            <a
              href="https://talent-workshop-graphql.herokuapp.com/graphiql"
              target="_blank"
              rel="noreferrer"
            >
              Try out GraphQL API here
            </a>
          </div>
          <RecommendedMovie movie={dataRecommendedMovie.findMovie} />
          <div>
            <h2 className="recommended-header">Other Movies</h2>
            <div className="select-movie-dropdown">
              <Select
                defaultValue={options[0]}
                onChange={(e) => {
                  setGenre(e.value);
                  setValue(e);
                }}
                options={options}
                value={value}
              />
            </div>
            <AllMovies selectedGenre={genre} />
          </div>
        </div>
      </div>
    </>
  );
};

export default MoviePage;
