import React, { useState } from 'react';

const Course = ({ course }) => {
  const [flipped, setFlipped] = useState('');
  const toggleFlip = () => {
    if (flipped === '') setFlipped('flipper-is-flipped');
    else setFlipped('');
  };
  const courseName = course.name.replace(/[^a-zA-z]/g, '').toLowerCase();
  return (
    <div
      key={course.id}
      className={`grid-box grid-box-${course.id} ${courseName}`}
      onClick={toggleFlip}
    >
      <div className="flipper">
        <div className={`flipper-card ${flipped}`}>
          <div className={`flipper-front ${courseName}-bg`}>
            <p>{course.name}</p>
          </div>
          <div className={`flipper-back ${courseName}-bg`}>
            <p className="description">{course.description}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Course;
