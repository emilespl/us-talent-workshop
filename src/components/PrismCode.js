import Prism from 'prismjs';
import { useEffect, useRef } from 'react';
import '../css/prism.css';

const PrismCode = ({ code, language = 'css' }) => {
  const myRef = useRef(null);
  useEffect(() => {
    hightlight();
  }, [code]);

  const hightlight = () => {
    if (myRef && myRef.current) {
      Prism.highlightElement(myRef.current);
    }
  };

  return (
    <pre>
      <code ref={myRef} className={`language-${language}`}>
        {code.trim()}
      </code>
    </pre>
  );
};

export default PrismCode;
