import React, { useState } from 'react';
import '../css/Grid.css';
import '../css/Playground.css';
import '../css/main.css';
import '../css/Dropdown.css';
import GridGap from '../components/grid-playground/GridGap';
import GridOverlay from '../components/grid-playground/GridOverlay';
import PrismCode from './PrismCode';
import Box from '../components/grid-playground/Box';
import BoxDropDown from './grid-playground/BoxDropDown';

const Grid = () => {
  const [selectedBox, setSelectedBox] = useState(0);
  const [gridGap, setGridGap] = useState('grid-gap-0');
  const [box1Column, setBox1Column] = useState('grid-column-1');
  const [box1Row, setBox1Row] = useState('grid-row-1');
  const [box2Column, setBox2Column] = useState('grid-column-2-4');
  const [box2Row, setBox2Row] = useState('grid-row-1-3');
  const [box3Column, setBox3Column] = useState('grid-column-1');
  const [box3Row, setBox3Row] = useState('grid-row-2-5');
  const [box4Column, setBox4Column] = useState('grid-column-3');
  const [box4Row, setBox4Row] = useState('grid-row-3');
  const [box5Column, setBox5Column] = useState('grid-column-2');
  const [box5Row, setBox5Row] = useState('grid-row-4');
  const [isGridLines, setIsGridlines] = useState(false);

  const handleClick = (value) => setSelectedBox(value);
  const resetBoxes = () => {
    setSelectedBox(0);
    setGridGap('grid-gap-0');
    setBox1Column('grid-column-1');
    setBox1Row('grid-row-1');
    setBox2Column('grid-column-2-4');
    setBox2Row('grid-row-1-3');
    setBox3Column('grid-column-1');
    setBox3Row('grid-row-2-5');
    setBox4Column('grid-column-3');
    setBox4Row('grid-row-3');
    setBox5Column('grid-column-2');
    setBox5Row('grid-row-4');
    setIsGridlines(false);
  };

  const trimGridGap = (value) => value.substring(9, value.length);
  const trimColumn = (value) =>
    value.substring(12, value.length).replace('-', ' / ');
  const trimRow = (value) =>
    value.substring(9, value.length).replace('-', ' / ');

  const gridCode = `
  .grid-container {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(4, 1fr);
  grid-gap: ${trimGridGap(gridGap)}px;
}

.box1 {
  grid-column: ${trimColumn(box1Column)};
  grid-row: ${trimRow(box1Row)};
}

.box2 {
  grid-column: ${trimColumn(box2Column)};
  grid-row: ${trimRow(box2Row)};
}

.box3 {
  grid-column: ${trimColumn(box3Column)};
  grid-row: ${trimRow(box3Row)};
}

.box4 {
  grid-column: ${trimColumn(box4Column)};
  grid-row: ${trimRow(box4Row)};
}

.box5 {
  grid-column: ${trimColumn(box5Column)};
  grid-row: ${trimRow(box5Row)};
}
    `;

  function getBoxData() {
    switch (selectedBox) {
      case 1:
        return {
          columnCb: setBox1Column,
          rowCb: setBox1Row,
          column: box1Column,
          row: box1Row
        };
      case 2:
        return {
          columnCb: setBox2Column,
          rowCb: setBox2Row,
          column: box2Column,
          row: box2Row
        };
      case 3:
        return {
          columnCb: setBox3Column,
          rowCb: setBox3Row,
          column: box3Column,
          row: box3Row
        };
      case 4:
        return {
          columnCb: setBox4Column,
          rowCb: setBox4Row,
          column: box4Column,
          row: box4Row
        };
      case 5:
        return {
          columnCb: setBox5Column,
          rowCb: setBox5Row,
          column: box5Column,
          row: box5Row
        };
      default:
        return {};
    }
  }

  return (
    <div className="section" id="grid">
      <h2>Grid</h2>
      <div
        className="flex flex-container-wrap-m"
        style={{ position: 'relative' }}
      >
        <GridOverlay isGridLines={isGridLines} />
        <div className={`grid-container ${gridGap}`}>
          <Box
            index={1}
            column={box1Column}
            row={box1Row}
            cb={() => handleClick(1)}
          />
          <Box
            index={2}
            column={box2Column}
            row={box2Row}
            cb={() => handleClick(2)}
          />
          <Box
            index={3}
            column={box3Column}
            row={box3Row}
            cb={() => handleClick(3)}
          />
          <Box
            index={4}
            column={box4Column}
            row={box4Row}
            cb={() => handleClick(4)}
          />
          <Box
            index={5}
            column={box5Column}
            row={box5Row}
            cb={() => handleClick(5)}
          />
        </div>
        <div className="flex column-fd dropdown-container">
          <GridGap onChange={setGridGap} />
          {selectedBox !== 0 ? (
            <>
              <BoxDropDown index={selectedBox} boxData={getBoxData()} />{' '}
            </>
          ) : null}
          <div>
            <label htmlFor="toggle-grid-lines">Toggle grid lines</label>
            <input
              type="checkbox"
              style={{ marginLeft: '10px' }}
              checked={isGridLines}
              onChange={() => setIsGridlines(!isGridLines)}
              id="toggle-grid-lines"
            />
          </div>
          <button className="reset-btn" onClick={resetBoxes}>
            Reset
          </button>
        </div>
      </div>
      <div style={{ marginTop: '16px' }}>
        <PrismCode code={gridCode} />
      </div>
    </div>
  );
};

export default Grid;
