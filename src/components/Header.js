import React, { useState } from 'react';
import { Link } from 'react-scroll';
import { Link as RouterLink, NavLink } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';
import { useLocation } from 'react-router-dom';
import '../css/Header.css';
//import './Header.scss';
import { ReactComponent as Logo } from '../assets/images/tele2_logo.svg';

const Header = () => {
  const location = useLocation();
  const [isOpen, setIsOpen] = useState(false);

  const PlaygroundMenuItems = () => {
    return (
      <div className="sub-menu-container">
        <div className="sub-menu">
          <Link className="header-text" to="flex" spy={true} smooth={true}>
            Flex
          </Link>
          <Link className="header-text" to="grid" spy={true} smooth={true}>
            Grid
          </Link>
        </div>
      </div>
    );
  };

  const MenuItems = ({ underlined }) => {
    return (
      <>
        <HashLink
          className="header-text router-header-text"
          to="/#talents"
          smooth={true}
          onClick={() => setIsOpen(!isOpen)}
        >
          Talents
        </HashLink>
        <HashLink
          className="header-text router-header-text"
          to="/#funfacts"
          smooth={true}
          onClick={() => setIsOpen(!isOpen)}
        >
          Fun Facts
        </HashLink>
        <HashLink
          className="header-text router-header-text"
          to="/#courses"
          smooth={true}
          onClick={() => setIsOpen(!isOpen)}
        >
          Courses
        </HashLink>
        <HashLink
          className="header-text router-header-text"
          to="/#brands"
          smooth={true}
          onClick={() => setIsOpen(!isOpen)}
        >
          Brands
        </HashLink>
        <NavLink
          className={`header-text router-header-text ${underlined}`}
          to={'/graphql'}
          onClick={() => setIsOpen(!isOpen)}
          activeClassName="router-underlined"
        >
          GraphQL
        </NavLink>
        <NavLink
          className={`header-text router-header-text`}
          to={'/playground'}
          onClick={() => setIsOpen(!isOpen)}
          activeClassName="router-underlined"
        >
          Playground
        </NavLink>
      </>
    );
  };

  return (
    <>
      <div className="header">
        <RouterLink className="header-logo" to={'/'}>
          <Logo title="logo" />
        </RouterLink>
        <div className="header-menu">
          {location.pathname === '/' ? <MenuItems /> : <MenuItems />}
        </div>
        <div className="mobile-menu">
          <div className="trigger-container">
            <div className="menu-trigger" onClick={() => setIsOpen(!isOpen)}>
              <span className="material-icons">
                {isOpen ? 'close' : 'menu'}
              </span>
            </div>
          </div>
          <div className={`mobile-menu-items ${isOpen ? 'visible' : ''}`}>
            <MenuItems />
          </div>
        </div>
      </div>
      {location.pathname === '/playground' && (
        <div className="sub-header">
          <PlaygroundMenuItems />
        </div>
      )}
    </>
  );
};

export default Header;
