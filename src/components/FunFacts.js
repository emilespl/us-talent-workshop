import React, { useEffect, useState } from 'react';
//import '../css/FunFacts.css';
import './FunFacts.scss';

const FunFacts = ({ funfacts }) => {
  const [isLoading, setIsLoading] = useState(true);

  const renderFunFacts = funfacts.map((funfact) => {
    return (
      <div key={funfact.id} className="fact-card">
        <p>{funfact.fact}</p>
      </div>
    );
  });

  const loadingBoxes = [...Array(4)];

  const renderLoading = loadingBoxes.map((element, i) => {
    return <div key={i} className="fact-card animated-background"></div>;
  });

  useEffect(() => {
    if (funfacts.length) {
      setIsLoading(false);
    }
  }, [funfacts]);

  return (
    <div className="section" id="funfacts">
      <h2>Fun Facts</h2>
      <div className="flex justify-space-b funfacts">
        {!isLoading ? renderFunFacts : renderLoading}
      </div>
    </div>
  );
};

export default FunFacts;
