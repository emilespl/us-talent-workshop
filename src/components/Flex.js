import React from 'react';
import { useState } from 'react';
import '../css/Header.css';
import '../css/Playground.css';
import '../css/Dropdown.css';
import AlignItems from './flex-playground/AlignItems';
import FlexDirection from './flex-playground/FlexDirection';
import JustifyContent from './flex-playground/JustifyContent';
import Wrap from './flex-playground/Wrap';
import PrismCode from './PrismCode';

const Flex = () => {
  const [direction, setDirection] = useState('row-fd');
  const [justify, setJustify] = useState('flex-start-jc');
  const [alignItems, setAlignItems] = useState('flex-start-ai');
  const [wrap, setWrap] = useState('no-wrap-fw');
  const box1Width = wrap !== 'no-wrap-fw' ? '70%' : '50%';
  const box1Height = wrap !== 'no-wrap-fw' ? '60%' : '40%';
  const [resetValue, setResetValue] = useState(false);

  const flexCode = `
.flex-container {
  display: flex;
  flex-direction: ${direction.substring(0, direction.length - 3)};
  justify-content: ${justify.substring(0, justify.length - 3)};
  align-items: ${alignItems.substring(0, alignItems.length - 3)};
  flex-wrap: ${wrap.substring(0, wrap.length - 3)};
}
  `;

  const resetBoxes = () => {
    setDirection('row-fd');
    setJustify('flex-start-jc');
    setAlignItems('flex-start-ai');
    setWrap('no-wrap-fw');
    setResetValue(true);
  };

  return (
    <div className="section" id="flex">
      <h2>Flex</h2>
      <div className="flex flex-container-wrap-m">
        <div
          className={`flex-playground-container ${direction} ${justify} ${alignItems} ${wrap}`}
        >
          <div
            className="box1"
            style={{ minWidth: box1Width, minHeight: box1Height }}
          ></div>
          <div className="box2" />
          <div className="box3" />
        </div>
        <div className="flex column-fd dropdown-container">
          <FlexDirection
            onChange={setDirection}
            reset={setResetValue}
            resetValue={resetValue}
          />
          <JustifyContent
            onChange={setJustify}
            reset={setResetValue}
            resetValue={resetValue}
          />
          <AlignItems
            onChange={setAlignItems}
            reset={setResetValue}
            resetValue={resetValue}
          />
          <Wrap
            onChange={setWrap}
            reset={setResetValue}
            resetValue={resetValue}
          />
          <button className="reset-btn" onClick={resetBoxes}>
            Reset
          </button>
        </div>
      </div>
      <div style={{ marginTop: '16px' }}>
        <PrismCode code={flexCode} />
      </div>
    </div>
  );
};

export default Flex;
