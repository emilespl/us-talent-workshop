import React, { useEffect, useState } from 'react';
import { gql, useQuery } from '@apollo/client';
import '../css/Movies.css';
import '../css/Playground.css';

const FIND_MOVIES = gql`
  query {
    findAllMovies {
      id
      title
      genre
      description
      posterUrl
    }
  }
`;

const FIND_MOVIES_BY_GENRE = gql`
  query($genre: String!) {
    findMoviesByGenre(genre: $genre) {
      id
      title
      genre
      description
      posterUrl
    }
  }
`;

const AllMovies = ({ selectedGenre }) => {
  const [movies, setMovies] = useState([]);

  const {
    loading: loadingAllMovies,
    error: errorAllMovies,
    data: dataAllMovies,
    refetch: refetchAllMovies
  } = useQuery(FIND_MOVIES, {
    onCompleted: () => {
      setMovies(dataAllMovies.findAllMovies);
    }
  });

  const {
    loading: loadingGenre,
    error: errorGenre,
    data: dataGenre,
    refetch: refetchByGenre
  } = useQuery(FIND_MOVIES_BY_GENRE, {
    variables: { genre: selectedGenre },
    onCompleted: () => {
      setMovies(dataGenre.findMoviesByGenre);
    }
  });

  useEffect(() => {
    if (selectedGenre === '' || selectedGenre === 'All') {
      if (!dataAllMovies) {
        refetchAllMovies();
      } else {
        setMovies(dataAllMovies.findAllMovies);
      }
    } else if (selectedGenre !== 'All' && selectedGenre !== '') {
      refetchByGenre({ variables: selectedGenre });
    }
  }, [selectedGenre]);

  if (loadingAllMovies || loadingGenre) return <p>Loading...</p>;
  if (errorAllMovies && errorGenre) return <p>Error :(</p>;

  const renderMovies = movies.map((movie) => {
    return (
      <div key={movie.title} className="movie">
        <img
          src={movie.posterUrl}
          alt={movie.title}
          style={{ width: '300px', height: '445px', objectFit: 'cover' }}
        />
        <p style={{ margin: '8px', fontSize: '18px', fontWeight: 'bold' }}>
          {movie.title}
        </p>
        <p className="movie-text">{movie.genre}</p>
        <p className="movie-text">{movie.description}</p>
      </div>
    );
  });

  return (
    <>
      <div className="flex flex-wrap">
        {movies ? renderMovies : 'Loading...'}
      </div>
    </>
  );
};

export default AllMovies;
