import React from 'react';
import '../../css/Grid.css';
import '../../css/GridOverlay.css';

let overlayPositionClasses = [];

for (let i = 1; i < 5; i++) {
  for (let m = 1; m < 4; m++) {
    overlayPositionClasses = [
      ...overlayPositionClasses,
      { boxClass: `${i}-${m}`, row: i, col: m }
    ];
  }
}

const renderOverlayBoxes = overlayPositionClasses.map((box) => {
  switch (box.boxClass) {
    case '1-1':
      return (
        <div
          className={`grid-pg-box overlay-box overlay-box-${box.boxClass}`}
          key={box.boxClass}
        >
          <div className="line-number">{box.col}</div>
          <div className="line-number">{box.row}</div>
        </div>
      );
    case '1-2':
      return (
        <div
          className={`grid-pg-box overlay-box overlay-box-${box.boxClass}`}
          key={box.boxClass}
        >
          <div className="line-number">{box.col}</div>
        </div>
      );
    case '1-3':
      return (
        <div
          className={`grid-pg-box overlay-box overlay-box-${box.boxClass}`}
          key={box.boxClass}
        >
          <div className="line-number">{box.col}</div>
          <div className="line-number">4</div>
        </div>
      );
    case '2-1':
    case '3-1':
      return (
        <div
          className={`grid-pg-box overlay-box overlay-box-${box.boxClass}`}
          key={box.boxClass}
        >
          <div className="line-number">{box.row}</div>
        </div>
      );
    case '4-1':
      return (
        <div
          className={`grid-pg-box overlay-box overlay-box-${box.boxClass}`}
          key={box.boxClass}
        >
          <div className="line-number">{box.row}</div>
          <div className="line-number">5</div>
        </div>
      );
    default:
      return (
        <div
          className={`grid-pg-box overlay-box overlay-box-${box.boxClass}`}
          key={box.boxClass}
        ></div>
      );
  }
});

const GridOverlay = ({ isGridLines }) => {
  if (isGridLines) {
    return <div className="grid-overlay">{renderOverlayBoxes}</div>;
  } else {
    return null;
  }
};

export default GridOverlay;
