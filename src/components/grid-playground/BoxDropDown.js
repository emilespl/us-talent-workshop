import React from 'react';
import Select from 'react-select';
import '../../css/Header.css';
import '../../css/Playground.css';

const BoxDropDown = ({ index, boxData }) => {
  const { columnCb, rowCb, column, row } = boxData;
  const handleColumn = (e) => columnCb(e.value);
  const handleRow = (e) => rowCb(e.value);

  const columnOptions = [
    { value: 'grid-column-1-3', label: '1/3' },
    { value: 'grid-column-2-4', label: '2/4' },
    { value: 'grid-column-1', label: '1' },
    { value: 'grid-column-2', label: '2' },
    { value: 'grid-column-3', label: '3' }
  ];

  const columnLabel = column.substring(12, column.length).replace('-', '/');

  const rowOptions = [
    { value: 'grid-row-1', label: '1' },
    { value: 'grid-row-2', label: '2' },
    { value: 'grid-row-3', label: '3' },
    { value: 'grid-row-4', label: '4' },
    { value: 'grid-row-1-3', label: '1/3' },
    { value: 'grid-row-2-5', label: '2/5' }
  ];

  const rowLabel = row.substring(9, row.length).replace('-', '/');

  return (
    <>
      <div className="flex column-fd text-align-l">
        <p className="dropdown-label">{`Box ${index} column`}</p>
        <Select
          onChange={handleColumn}
          options={columnOptions}
          value={{ value: column, label: columnLabel }}
          defaultValue={{ value: column, label: columnLabel }}
        />
      </div>
      <div className="flex column-fd text-align-l">
        <p className="dropdown-label">{`Box ${index} row`}</p>
        <Select
          onChange={handleRow}
          options={rowOptions}
          value={{ value: row, label: rowLabel }}
          defaultValue={{ value: row, label: rowLabel }}
        />
      </div>
    </>
  );
};

export default BoxDropDown;
