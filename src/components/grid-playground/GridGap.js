import React from 'react';
import Select from 'react-select';
import '../../css/Header.css';
import '../../css/Playground.css';

const GridGap = ({ onChange }) => {
  const handleGridGap = (e) => onChange(e.value);

  const options = [
    { value: 'grid-gap-0', label: '0 px' },
    { value: 'grid-gap-4', label: '4 px' }
  ];

  return (
    <>
      <div className="flex column-fd text-align-l">
        <p className="dropdown-label">Grid gap</p>
        <Select
          defaultValue={options[0]}
          onChange={handleGridGap}
          options={options}
        />
      </div>
    </>
  );
};

export default GridGap;
