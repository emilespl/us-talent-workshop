import React from 'react';
import '../../css/Header.css';
import '../../css/Playground.css';

const Box = ({ index, column, row, cb }) => {
  const handleClick = () => cb(index);

  return (
    <div className={`grid-pg-box grid-pg-box-${index} ${column} ${row}`} onClick={() => handleClick()}>Box {index}</div>
  );
}

export default Box;