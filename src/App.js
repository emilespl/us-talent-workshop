import './App.css';
import './css/main.css';
//import './scss/main.scss';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Home from './components/Home';
import Playground from './components/Playground';
import Header from './components/Header';
import Brands from './components/Brands';
import spring from './api/spring';
import MoviePage from './components/MoviePage';

function App() {
  const [brands, setBrands] = useState([]);

  const brandData = async () => {
    const response = await spring.get('/brands/all');
    const { data } = response;
    setBrands(data);
  };

  useEffect(() => {
    brandData();
  }, []);

  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/playground" component={Playground} />
          <Route path="/graphql" component={MoviePage} />
        </Switch>
        <div className="footer">
          <Brands brands={brands} />
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
