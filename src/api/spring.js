import axios from 'axios';

export default axios.create({
  baseURL: 'https://talent-t2-api.herokuapp.com'
});
